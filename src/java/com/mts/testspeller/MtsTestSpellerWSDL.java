
package com.mts.testspeller;

import java.util.Random;
import javax.ejb.Stateless;
import javax.jws.WebService;
import net.yandex.speller.services.spellservice.CheckTextResponse;



@WebService(serviceName = "SpellService", portName = "SpellServiceSoap")
@Stateless
public class MtsTestSpellerWSDL {

    public net.yandex.speller.services.spellservice.CheckTextResponse checkText(net.yandex.speller.services.spellservice.CheckTextRequest parameters) {
       
        if(parameters==null)
            throw new UnsupportedOperationException("Null parametrs.");
        if(parameters.getText()==null)
            throw new UnsupportedOperationException("Null text.");
        if("".equals(parameters.getText()))
            throw new UnsupportedOperationException("Text empty");
            
        String [] words = parameters.getText().split(" +");
        int wordsCount = words.length;
       
        Random rnd = new Random();
        int mistakes = rnd.nextInt(6)%wordsCount;
        
        MySpellResult sr = new MySpellResult();
        
        if(mistakes>0)
            for (int i = 0; i <= mistakes; i++) {
                MySpellError mse = new MySpellError();
                String word = words[rnd.nextInt(wordsCount)];
                mse.setWord(word);
                mse.addError(word.toUpperCase());
                sr.addError(mse);
            }
        
        CheckTextResponse ctr = new CheckTextResponse();
        ctr.setSpellResult(sr);
        return ctr;
   }

    public net.yandex.speller.services.spellservice.CheckTextsResponse checkTexts(net.yandex.speller.services.spellservice.CheckTextsRequest parameters) {
        //TODO implement this method
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
}
