
package com.mts.testspeller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import net.yandex.speller.services.spellservice.SpellError;


public class MySpellError extends SpellError{

    public MySpellError() {
        super();
        Random rnd = new Random();
        code = rnd.nextInt(10);
        col = rnd.nextInt(10); 
        len = rnd.nextInt(10);
        pos = rnd.nextInt(10);
        row = rnd.nextInt(10);
        s = new ArrayList<>();
    }

    public void setS(List<String> str){
        s = str;
    }
    public void addError(String err){
        s.add(err);
    }
}
