
package com.mts.testspeller;

import java.util.ArrayList;
import net.yandex.speller.services.spellservice.SpellError;
import net.yandex.speller.services.spellservice.SpellResult;


public class MySpellResult extends SpellResult{

    public MySpellResult() {
        super();
        error = new ArrayList<SpellError>();
    }
    public void addError(SpellError se){
        error.add(se);
    }
    
}
